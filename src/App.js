import logo from './logo.svg';
import './App.css';

import cartoon from './nexus';

function App() {
  return (
  <div>
      <h1>Welcome to the course booking batch 164</h1>
      <h5>This is our project in React</h5>
      <h6> Come visit our website</h6>
    {/*This image came from the /src folder*/}
      <img src={cartoon} alt="image not found" />
    {/*Lets try to display an image but this time it will come from the public folder */}
      <img src="/nexus"/>
    {/*the resource used on line 15 camme from the public folder, so it can be used directly by the module*/}
      <h3>This is My Favorite Cartoon Character</h3>

  </div>
  );
}

export default App;
